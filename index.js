/** Bài tập 1
 * 
 */

function timSoNguyenDuongNhoNhat () {
    var soNguyenDuongNhoNhat = 0;
    var tongsoNguyen = 0;
    while (tongsoNguyen < 10000) {
        soNguyenDuongNhoNhat++;
tongsoNguyen = tongsoNguyen + soNguyenDuongNhoNhat;
    }
    return soNguyenDuongNhoNhat;
}

document.getElementById('ketquaSoNguyenDuongNhoNhat').onclick = function () {
    document.getElementById('showKetQua').innerHTML = 'Số nguyên dương nhỏ nhất là: ' +  timSoNguyenDuongNhoNhat();
}


/** Bài tập 2
 * 
 */

function tinhTongSn() {
   var soXduocnhap = document.getElementById('soX').value*1;
   var soNduocnhap = document.getElementById('soN').value*1;
   var Tong = 0;
   for (var i = 1; i<= soNduocnhap; i++) {
    Tong +=Math.pow(soXduocnhap,i);
   }
   return Tong;
}

document.getElementById('tinhTong').onclick = function () {
    document.getElementById('tinhTongS').innerHTML = 'Tổng S(n) = ' +  tinhTongSn();
}
  
/** Bài tập 3
*
 */

function timGiaiThua() {
    var giaiThua = 1;
var soNbai13 = document.getElementById('soNbai3').value*1;
for (var i = 1; i <= soNbai13; i++) {
    giaiThua *= i;
}
return giaiThua;
}

document.getElementById('tinhGiaiThua').onclick = function () {
    document.getElementById('ketquaGiaiThua').innerHTML = 'Kết quả giai thừa là: ' +  timGiaiThua();
}


/** Bài tập 4
 * 
 */

function taoDiv () {
    var content ='';
    for (var i = 1; i<=10; i++) {
        if (i % 2 == 0) {
            content += '<div class="bg-danger bg-opacity-75 text-white mt-3 py-3"></div>';
        } else { content += '<div class="bg-primary bg-opacity-75 text-white mt-3 py-3"></div>'; 
    }
    }
    return content;
}


document.getElementById('taoTheDiv').onclick = function () {
        document.getElementById('tao10theDiv').innerHTML = taoDiv();
}